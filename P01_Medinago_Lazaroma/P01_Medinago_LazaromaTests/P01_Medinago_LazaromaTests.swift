//
//  P01_Medinago_LazaromaTests.swift
//  P01_Medinago_LazaromaTests
//
//  Created by pvesat on 28/11/17.
//  Copyright © 2017 Medinago_Lazaroma. All rights reserved.
//

import XCTest
@testable import LoRAPI


@testable import P01_Medinago_Lazaroma

class P01_Medinago_LazaromaTests: XCTestCase {
    
    func testReadFromJson(){
        LoRAPI().getDictionary(url: "https://www.jasonbase.com/things/K0zr.json") {
            jsonObj in
            
            XCTAssertNil(jsonObj)
        }
        
    }
    func testParse(){
        
        var appUsers: [User] = []
        
        LoRAPI().getDictionary(url: "https://www.jasonbase.com/things/K0zr.json") {
            jsonObj in
            
            if let usersArray = jsonObj!.value(forKey: "users") as? NSArray {
                usersArray.forEach { user in
                    
                    if let userDict = user as? NSDictionary {
                        if let userName = (userDict.value(forKey: "username") as? String),
                            let userPassword = (userDict.value(forKey: "password") as? String),
                            let userHero = (userDict.value(forKey: "hero") as? String){
                            
                            appUsers.append(User(
                                username: userName,
                                password: userPassword,
                                hero: userHero
                            ))
                            
                            XCTAssertNil(appUsers)
                        }
                    }
                }
            }
        }
    }
}
