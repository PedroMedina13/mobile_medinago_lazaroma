//
//  MenuOption.swift
//  P01_Medinago_Lazaroma
//
//  Created by pvesat on 28/11/17.
//  Copyright © 2017 Medinago_Lazaroma. All rights reserved.
//

import Foundation

struct MenuOption{
  
  var section_name: String
  var image: String
  
    init(section_name: String, image: String){
    self.section_name = section_name
    self.image = image
  }
  
}
