//
//  User.swift
//  P01_Medinago_Lazaroma
//
//  Created by pvesat on 28/11/17.
//  Copyright © 2017 Medinago_Lazaroma. All rights reserved.
//

import Foundation

struct User{
  
  var username: String
  var password: String
  var hero: String
  
    init(username: String, password: String, hero: String){
    self.username = username
    self.password = password
    self.hero = hero
  }
  
}
