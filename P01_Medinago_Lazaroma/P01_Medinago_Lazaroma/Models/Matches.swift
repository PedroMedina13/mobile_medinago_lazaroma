//
//  Matches.swift
//  P01_Medinago_Lazaroma
//
//  Created by pvesat on 28/11/17.
//  Copyright © 2017 Medinago_Lazaroma. All rights reserved.
//

import Foundation

struct Matches{
    
    var map: String
    var hero: String
    var matchState: String
    var username: String
    
    init(map: String, hero: String, matchState: String, username: String){
        
        self.map = map
        self.hero = hero
        self.matchState = matchState
        self.username = username
    }
    
}
