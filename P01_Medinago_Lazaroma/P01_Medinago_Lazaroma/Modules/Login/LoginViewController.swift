//
//  LoginViewController.swift
//  P01_Medinago_Lazaroma
//
//  Created by pvesat on 27/11/17.
//  Copyright © 2017 Medinago_Lazaroma. All rights reserved.
//
//
//  Created by Pedro Medina
//

import UIKit
import Toast_Swift
import LoRAPI

class LoginViewController: UIViewController {
  
    @IBOutlet weak var backgroundImage: UIImageView!
    @IBOutlet weak var logoImage: UIImageView!
    @IBOutlet weak var loginButton: UIButton!
    
    @IBOutlet weak var userText: UITextField!
    
    @IBOutlet weak var passwordText: UITextField!
    
    @IBOutlet weak var userLabel: UILabel!
    
    @IBOutlet weak var passwordLabel: UILabel!
    
  var appUsers: [User] = []
  var currentUser: User!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    // Do any additional setup after loading the view.
    backgroundImage.image = UIImage(named: "Background")
    logoImage.image = UIImage(named: "Logo")
    userLabel.text = "Username"
    passwordLabel.text = "Password"
    passwordText.isSecureTextEntry = true

    parseUsers(url: "https://www.jasonbase.com/things/K0zr.json")
    loginButton.addTarget(self, action: #selector(self.CheckLogin), for:.touchUpInside)
  }
  
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }
  @objc func CheckLogin(){
    let validUser = self.appUsers.filter { $0.username == userText.text && $0.password == passwordText.text}.first
    
    
    
    
    guard validUser != nil else {
        
        self.navigationController?.view.makeToast("Wrong username or password!", duration: 2.0, position: .top)
        return
    }
    
    
    let defaults = UserDefaults.standard
    let dict = ["username": validUser?.username, "password": validUser?.password, "hero": validUser?.hero]
    defaults.set(dict, forKey: "CurrentUser")
    
    let storyboard = UIStoryboard(name: "Main", bundle: nil)
    let nv = storyboard.instantiateViewController(withIdentifier: "MenuViewController") as! MenuViewController
    navigationController?.pushViewController(nv,animated: true)

  }
    func parseUsers(url: String){
        
        LoRAPI().getDictionary(url: url) {
            jsonObj in
            
            if let usersArray = jsonObj!.value(forKey: "users") as? NSArray {
                usersArray.forEach { user in
                    
                    if let userDict = user as? NSDictionary {
                        if let userName = (userDict.value(forKey: "username") as? String),
                            let userPassword = (userDict.value(forKey: "password") as? String),
                            let userHero = (userDict.value(forKey: "hero") as? String){
                            
                            self.appUsers.append(User(
                                username: userName,
                                password: userPassword,
                                hero: userHero
                            ))
                            
                            
                            
                        }
                    }
                }
            }
            
        }
        
    }

}

