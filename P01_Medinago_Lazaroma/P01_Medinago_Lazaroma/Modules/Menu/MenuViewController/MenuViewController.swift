//
//  MenuViewController.swift
//  P01_Medinago_Lazaroma
//
//  Created by pvesat on 28/11/17.
//  Copyright © 2017 Medinago_Lazaroma. All rights reserved.
//
//
//  Created by Pedro Medina
//

import UIKit

class MenuViewController: UIViewController {

  @IBOutlet weak var tableView: UITableView!
  
  var options: [MenuOption] = []
  
  override func viewDidLoad() {
        super.viewDidLoad()

    setupTable()
    configureOptions()
    
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
  
  private func setupTable(){
    self.tableView.delegate = self
    self.tableView.dataSource = self
    
    self.tableView.separatorStyle = .none
    self.tableView.rowHeight = 168
    self.tableView.estimatedRowHeight = 50
    
    let menuCellNib = UINib(nibName: "MenuTableViewCell", bundle: nil)
    self.tableView.register(menuCellNib, forCellReuseIdentifier: "MenuTableViewCell")
  }

  private func configureOptions(){
    
    var opt = MenuOption(section_name: "Matches", image: "Ranking")
    options.append(opt)
    opt = MenuOption(section_name: "Skins", image: "Skins")
    options.append(opt)
    opt = MenuOption(section_name: "Contact", image: "Contact")
    options.append(opt)
  }
}

extension MenuViewController : UITableViewDelegate{
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        
        if(indexPath.row == 0){
            let nv = storyboard.instantiateViewController(withIdentifier: "MatchViewController") as! MatchViewController
            navigationController?.pushViewController(nv,animated: true)
        }
        else if(indexPath.row == 1){
            let nv = storyboard.instantiateViewController(withIdentifier: "SkinsViewController") as! SkinsViewController
            navigationController?.pushViewController(nv,animated: true)
        }
        else if(indexPath.row == 2){
            let nv = storyboard.instantiateViewController(withIdentifier: "ContactInfoViewController") as! ContactInfoViewController
            navigationController?.pushViewController(nv,animated: true)
        }
    }
  
}

extension MenuViewController : UITableViewDataSource{
  
  func numberOfSections(in tableView: UITableView) -> Int {
    return 1
  }
  
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return self.options.count
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    
    let option = self.options[indexPath.row]
    
    return self.configureMenuOption(option: option)
    
  }
  
  func configureMenuOption(option: MenuOption) -> UITableViewCell{
    
    guard let cell = self.tableView.dequeueReusableCell(withIdentifier: "MenuTableViewCell") as? MenuTableViewCell else{
        return UITableViewCell()
    }
    
    cell.optionLabel.text = option.section_name
    cell.optionImage.image = UIImage(named: option.image)
    cell.optionImage.alpha = 1
    
    return cell
    
  }
  
  
}
