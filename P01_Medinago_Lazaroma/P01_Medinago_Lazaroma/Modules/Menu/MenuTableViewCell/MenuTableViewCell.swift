//
//  MenuTableViewCell.swift
//  P01_Medinago_Lazaroma
//
//  Created by pvesat on 28/11/17.
//  Copyright © 2017 Medinago_Lazaroma. All rights reserved.
//
//
//  Created by Pedro Medina
//

import UIKit

class MenuTableViewCell: UITableViewCell {


  @IBOutlet weak var optionImage: UIImageView!
  @IBOutlet weak var optionLabel: UILabel!
  
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
