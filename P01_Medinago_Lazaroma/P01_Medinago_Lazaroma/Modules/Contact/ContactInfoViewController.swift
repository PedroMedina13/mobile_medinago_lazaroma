//
//  ContactInfoViewController.swift
//  P01_Medinago_Lazaroma
//
//  Created by pvesat on 30/11/17.
//  Copyright © 2017 Medinago_Lazaroma. All rights reserved.
//
//  Created by Andres Lazaro
//


import UIKit
import MessageUI

class ContactInfoViewController: UIViewController, MFMailComposeViewControllerDelegate{

    @IBOutlet var companyName: UILabel!
    @IBOutlet weak var backgroundImage: UIImageView!
    @IBOutlet var facebookButton: UIButton!
    @IBOutlet var facebookLabel: UILabel!
    @IBOutlet var twitterButton: UIButton!
    @IBOutlet var twitterLabel: UILabel!
    
    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var messageText: UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        backgroundImage.image = UIImage(named: "Background")
        companyName.text = "Shelby Ltd"
        companyName.textColor = UIColor.white
        facebookLabel.text = "Facebook"
        facebookLabel.textColor = UIColor.white
        twitterLabel.text = "Twitter"
        twitterLabel.textColor = UIColor.white
        messageLabel.textColor = UIColor.white
        messageLabel.text = "Message us!"
        
        
        facebookButton.addTarget(self, action: #selector(self.loadFacebook), for:.touchUpInside)
        twitterButton.addTarget(self, action: #selector(self.loadTwitter), for:.touchUpInside)
        
        if !MFMailComposeViewController.canSendMail(){
            print("Mail services are not available")
            return
        }
        
        
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @objc func loadFacebook(){
        guard let url_dir = URL(string: "https://www.facebook.com") else {
            return
        }
        UIApplication.shared.open(url_dir, options: [:], completionHandler: nil)
    }
    @objc func loadTwitter(){
        guard let url_dir = URL(string: "https://www.twitter.com") else {
            return
        }
        UIApplication.shared.open(url_dir, options: [:], completionHandler: nil)
    }
    @IBAction func sendMessageButtonTaped(_ sender: Any){
        
        let composeVC = MFMailComposeViewController()
        composeVC.mailComposeDelegate = self
        
        // Configure the fields of the interface.
        composeVC.setToRecipients(["management@shelbyltd.com"])
        composeVC.setSubject("Contact message")
        composeVC.setMessageBody(messageText.text, isHTML: false)
        
        // Present the view controller modally.
        self.present(composeVC, animated: true, completion: nil)
        
        
        
        
        
        
        
    }
}
