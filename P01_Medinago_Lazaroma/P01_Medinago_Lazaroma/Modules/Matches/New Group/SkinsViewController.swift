//
//  SkinsViewController.swift
//  P01_Medinago_Lazaroma
//
//  Created by pvesat on 29/11/17.
//  Copyright © 2017 Medinago_Lazaroma. All rights reserved.
//
//
//  Created by Pedro Medina
//

import UIKit

class SkinsViewController: UIViewController {

    @IBOutlet weak var backgroundImage: UIImageView!
    
    @IBOutlet weak var heroName: UILabel!
    @IBOutlet weak var heroImage: UIImageView!
    
    @IBOutlet weak var skin1: UIImageView!
    @IBOutlet weak var weapon1: UIImageView!
    
    @IBOutlet weak var skin2: UIImageView!
    @IBOutlet weak var weapon2: UIImageView!
    
    @IBOutlet weak var skin3: UIImageView!
    @IBOutlet weak var weapon3: UIImageView!
    
    
    @IBOutlet var skinsText: UILabel!
    @IBOutlet var weaponText: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        backgroundImage.image = UIImage(named: "Background")
        heroName.textColor = UIColor.white
        skinsText.textColor = UIColor.white
        weaponText.textColor = UIColor.white
        
        load()

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    private func load(){
        let defaults = UserDefaults.standard
        let heroDict = defaults.object(forKey: "CurrentUser") as? NSDictionary
        let hero = heroDict?.object(forKey: "hero") as? String
        
        if(hero == "Sombra"){
            heroName.text = hero
            print(heroName.text!)
            
            heroImage.image = UIImage(named: "SombraMain")

            skin1.image = UIImage(named: "SombraSkin1")
            skin2.image = UIImage(named: "SombraSkin2")
            skin3.image = UIImage(named: "SombraSkin3")
            
            weapon1.image = UIImage(named: "SombraWeapon1")
            weapon2.image = UIImage(named: "SombraWeapon2")
            weapon3.image = UIImage(named: "SombraWeapon3")
            
        }
        else if(hero == "Tracer"){
            heroName.text = hero
            print(heroName.text!)
            
            heroImage.image = UIImage(named: "TracerMain")
            
            skin1.image = UIImage(named: "TracerSkin1")
            skin2.image = UIImage(named: "TracerSkin2")
            skin3.image = UIImage(named: "TracerSkin3")
            
            weapon1.image = UIImage(named: "TracerWeapon1")
            weapon2.image = UIImage(named: "TracerWeapon2")
            weapon3.image = UIImage(named: "TracerWeapon3")
        }
    }
}
