//
//  MatchViewController.swift
//  P01_Medinago_Lazaroma
//
//  Created by pvesat on 29/11/17.
//  Copyright © 2017 Medinago_Lazaroma. All rights reserved.
//
//
//  Created by Andres Lazaro
//

import UIKit
import LoRAPI

class MatchViewController: UIViewController {

    @IBOutlet weak var matchTableView: UITableView!
    var matches: [Matches] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        setupTable()
        configMatches()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    private func setupTable(){
        self.matchTableView.delegate = self
        self.matchTableView.dataSource = self
        
        self.matchTableView.separatorStyle = .none
        self.matchTableView.rowHeight = 168
        self.matchTableView.estimatedRowHeight = 50
        
        let matchCellNib = UINib(nibName: "MatchesTableViewCell", bundle: nil)
        self.matchTableView.register(matchCellNib, forCellReuseIdentifier: "MatchesTableViewCell")
    }
    
    private func configMatches(){
        parseMatches(url: "https://www.jasonbase.com/things/B1xp.json")
    }
    private func parseMatches(url: String){
        
        LoRAPI().getDictionary(url: url) {
            jsonObj in
            
            
            let defaults = UserDefaults.standard
            let heroDict = defaults.object(forKey: "CurrentUser") as? NSDictionary
            let user = heroDict?.object(forKey: "username") as? String
            
            
            
            if let matchesArray = jsonObj!.value(forKey: "matches") as? NSArray {
                matchesArray.forEach { match in
                    
                    if let matchesDict = match as? NSDictionary {
                        if let matchMap = (matchesDict.value(forKey: "map") as? String),
                            let matchHero = (matchesDict.value(forKey: "hero") as? String),
                            let matchstate = (matchesDict.value(forKey: "result") as? String),
                            let matchUser = (matchesDict.value(forKey: "username") as? String) {
                            
                            if(user == matchUser){
                                self.matches.append(Matches(map: matchMap, hero: matchHero, matchState: matchstate, username: matchUser))
                            }
                        }
                    }
                    
                }
                DispatchQueue.main.async {
                    self.matchTableView.reloadData()
                }
            }
        }
    }
}
extension MatchViewController : UITableViewDelegate{
    
    
    
    
}
extension MatchViewController : UITableViewDataSource{
    
    func numberOfSections(in matchTableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.matches.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let match = self.matches[indexPath.row]
        
        return self.configureMatchOption(match: match)
        
    }
    
    func configureMatchOption(match: Matches) -> UITableViewCell{
        
        guard let cell = self.matchTableView.dequeueReusableCell(withIdentifier: "MatchesTableViewCell") as? MatchesTableViewCell else{
            return UITableViewCell()
        }
        
        cell.backgroundImage.image = UIImage(named: match.map)
        cell.heroImage.image = UIImage(named: match.hero)
        cell.matchState.text = match.matchState
        if(match.matchState == "WON"){
            cell.matchState.textColor = UIColor.green
        }
        else if(match.matchState == "LOST"){
            cell.matchState.textColor = UIColor.red
        }
        
        
        return cell
        
    }
    
    
}
